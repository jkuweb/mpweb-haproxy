# ![La Salle BES](http://jcarreras.es/images/lasalle.png)

# ![screenshot](.screenshot.gif)

# Descripción
-----------------------

Experimenta con `haproxy`, los backends y su panel de `stats`.

En este ejercicio hay instalados dos servidores web, apache y nginx, escuchando en los puertos 8080 y 8090 respectivamente.
Además, hay ya configurado el balanceador `haproxy` preparado para balancear peticiones de un servidor al otro.

Sigue los pasos para experimentar con el funcionamiento de un balanceador de carga.


# Instalación
-----------------------

```
$ vagrant up
```


# Instrucciones
-----------------------


### Probando el balanceo

- Entra en `http://1.2.3.4:8080` para ver el contenido servido por Apache
- Entra en `http://1.2.3.4:8090` para ver el contenido servido por Nginx
- Imaginemos que la aplicación es la misma. Con fines didácticos cada servidor muestra en el HTML su nombre, para poder distinguir qué servidor está devolviendo las peticiones pero en condiciones normales los servidores devolverían el mismo HTML
- Entra en la máquina ejecutando un `vagrant ssh`
- Abre el fichero `/etc/default/haproxy` para ver como `haproxy` ha sido configurado. La parte interesante está al final del fichero
- Hazte super usuario con el comando `sudo su -`
- Abre el fichero `/etc/default/haproxy` y modifica el valor de `ENABLED` a `1`
- Inicia el servicio de `haproxy` ejecutando `service haproxy start`
- Entra en `http://1.2.3.4/` . Recarga varias veces y observa el comportamiento

### Viendo el estado de `haproxy`

- Entra en `http://1.2.3.4/haproxy?stats`
- Podrás ver las estadísticas de uso para cada uno de los backends
- Recarga la página `http://1.2.3.4/` para ver como los valores de `Statistics Report` aumenta

### Viendo qué pasa al parar uno de los servidores web

- Ejecuta, como root, `service apache2 stop` para parar el servicio de apache2.
- Recarga la página `http://1.2.3.4/` repetidas veces observando el comportamiento
- Entra en http://1.2.3.4/haproxy?stats y observa el estado de los backends
- Ejecuta, como root, `service apache2 ngin` para parar el servicio de apache2.
- Entra en `http://1.2.3.4/haproxy?stats` y observa el estado de los backends


# Desinstalación
-----------------------

```
$ vagrant destroy
```
